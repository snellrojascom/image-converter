Templates Prompts

Code for index.html file create a div and add these 5 links as a list in it with href= appropriate route from our flask server.
JPG to PNG converter
PNG to JPG converter
WEBP to PNG converter
BMP to PNG converter
PNG to PDF converter

Code for jpgtopng.html file.
Display input form, so users can select an image from their computer.
Also, add a "Convert to PNG" button.
Onclick button, check if the image is selected and the image is in jpg format and less than 5 MB.
If true, send a POST request to route "api/jpgtopng" with the image as a parameter.
Else show relevant errors like “select an image” or “image is not in jpg format”

Code for pngtojpg.html file.
Display input form, so users can select an image from their computer.
Also, add a "Convert to JPG" button.
On the click button, check if the image is selected and the image is in png format and less than 5 MB 
if true, send a POST request to route "/api/pngtojpg" with the image as a parameter. 
else show relevant errors like “select an image” or “the image is not in png format”

Code for webptopng.html file.
Display input form, so users can select an image from their computer. 
Also, add a "Convert to PNG" button.
On the click button, check if the image is selected and the image is in webp format and less than 5 MB. 
if true, send a POST request to route "/api/webptopng" with the image as a parameter. 
else show relevant errors like “select an image” or “the image is not in WEBP format”.

Code for bmptopng.html file. 
Display input form, so users can select an image from their computer.
Also, add a "Convert to PNG" button.
On the click button, check if the image is selected and the image is in bmp format and less than 5 MB. 
if true, send a POST request to route "/api/baptopng" with the image as a parameter. 
else show relevant errors like “select an image” or “image is not in bmp format”.

Code for pngtopdf.html file.
Display input form, so users can select an image from their computer. 
Also, add a "Convert to PDF" button.
On the click button, check if the image is selected and the image is in png format and less than 5 MB. 
if true, send a POST request to route "/api/pngtond" with the image as a parameter.
else show relevant errors like “select an image” or “the image is not in png format”

Server-Side Prompts

For the flask server, create a route “/api/jpgtopng” with the POST method.
Take the image from the parameter and convert it into PNG.
Don’t save the image on the server, just return the PNG image as a downloadable attachment using io.BytesIO().

For the flask server, create a route “/api/pngtojpg” with a POST method.
Convert the PNG image from the parameter into JPG format.
Don’t save the image on the server, just return the JPG image as a downloadable attachment.

For the flask server, create a route “/api/webptopng” with a POST method.
Convert the WEBP image from the parameter into PNG format.
Don’t save the image on the server, just return the PNG image as a downloadable attachment.

For the flask server, create a route “/api/bmptopng” with a POST method.
Convert the BMP image from the parameter into PNG format.
Don’t save the image on the server, just return the PNG image and download it.

For the flask server, create a route “/api/pngtopdf” with a POST method.
Convert the PNG image from the parameter into PDF format.
Don’t save the PDF file on the server, just return the PDF file as a downloadable attachment.
Styles CSS

CSS code for pngtopdf.html file displays the form input in the middle and add some background color

Add styles to the form element or the input element to change the appearance of the form. and CSS for convert button
