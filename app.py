from flask import Flask, render_template, request, Response, make_response, send_file
from PIL import Image
from io import BytesIO

app = Flask(__name__)

@app.route("/", methods=["GET"])
def index():
    return render_template("index.html")

@app.route("/jpgtopng", methods=["GET"])
def jpgtopng():
    return render_template("jpgtopng.html")

@app.route("/pngtojpg", methods=["GET"])
def pngtojpg():
    return render_template("pngtojpg.html")

@app.route("/webptopng", methods=["GET"])
def webptopng():
    return render_template("webptopng.html")

@app.route("/bmptopng", methods=["GET"])
def bmptopng():
    return render_template("bmptopng.html")

@app.route("/pngtopdf", methods=["GET"])
def pngtopdf():
    return render_template("pngtopdf.html")

@app.route("/api/jpgtopng", methods=["POST"])
def jpg_to_png():
    image = request.files["image"]
    image = Image.open(image)
    image = image.convert("RGB")

    # Create a BytesIO object to hold the PNG image data
    image_data = BytesIO()
    image.save(image_data, 'PNG')

    # Reset the file pointer to the beginning of the file
    image_data.seek(0)

    #################
    # Create the response object and set headers
    #response = make_response(image_data.read())
    #response.headers["Content-Type"] = "image/png"
    #response.headers["Content-Length"] = len(image_data.getvalue())
    #response.headers["Content-Disposition"] = "attachment; filename=converted_from_jpg.png"
    #return response
    

    #################
    """
    return send_file(image_data,
                    mimetype='image/png', 
                    as_attachment=True, 
                    download_name='converted_from_jpg.png')
    """

    ########

    response = Response(image_data, content_type="image/png")
    response.headers.set("Content-Disposition", "attachment", filename="converted_from_jpg.png")
    return response

@app.route("/api/pngtojpg", methods=["POST"])
def png_to_jpg():
    image = request.files["image"]
    image = Image.open(image)
    image = image.convert("RGB")

    # Create a BytesIO object to hold the PNG image data
    image_data = BytesIO()
    image.save(image_data, format="JPEG")

    # Reset the file pointer to the beginning of the file
    image_data.seek(0)

    # Create the response object and set headers
    response = make_response(image_data.read())
    response.headers["Content-Type"] = "image/jpeg"
    response.headers["Content-Disposition"] = "attachment; filename=converted_from_png.png"
    return response

@app.route("/api/webptopng", methods=["POST"])
def webp_to_png():
    image = request.files["image"]
    image = Image.open(image)
    image = image.convert("RGB")
    
    # Create a BytesIO object to hold the PNG image data
    image_data = BytesIO()
    image.save(image_data, format="PNG")

    # Reset the file pointer to the beginning of the file
    image_data.seek(0)

    # Create the response object and set headers
    response = make_response(image_data.read())
    response.headers["Content-Type"] = "image/png"
    response.headers["Content-Disposition"] = "attachment; filename=converted_from_webp.png"
    return response

@app.route("/api/bmptopng", methods=["POST"])
def bmp_to_png():
    image = request.files["image"]
    image = Image.open(image)
    
    # Create a BytesIO object to hold the PNG image data
    image_data = BytesIO()
    image.save(image_data, format="PNG")

    # Reset the file pointer to the beginning of the file
    image_data.seek(0)

    # Create the response object and set headers
    response = make_response(image_data.read())
    response.headers["Content-Type"] = "image/png"
    response.headers["Content-Disposition"] = "attachment; filename=converted_from_bmp.png"
    return response

@app.route("/api/pngtopdf", methods=["POST"])
def png_to_pdf():
    image = request.files["image"]
    image = Image.open(image)
    image = image.convert("RGB")

    # Create a BytesIO object to hold the PNG image data
    image_data = BytesIO()
    image.save(image_data, format="PDF")

    # Reset the file pointer to the beginning of the file
    image_data.seek(0)

    # Create the response object and set headers
    response = make_response(image_data.read())
    response.headers["Content-Type"] = "application/pdf"
    response.headers["Content-Disposition"] = "attachment; filename=converted_from_png.pdf"
    return response
    

if __name__ == "__main__":
    app.run(debug=True)
